package Ex1;

import java.time.Instant;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.Calendar;
import java.text.SimpleDateFormat;


public class Prescricao {

    private Remedio remedio;
    public Date dataInicioValidade;
    public Date dataFimValidade;
    public Date dataTerminioUso;
    public Date dataInicioUso;
    public int qtdVezesAoDia;
    public String dosagem;

    private LinkedList<HorarioRemedio> horas = new LinkedList<HorarioRemedio>();

    public Date getDataInicioValidade() {
        return dataInicioValidade;
    }

    public void setDataInicioValidade(Date dataInicioValidade) {
        this.dataInicioValidade = dataInicioValidade;
    }

    public Date getDataFimValidade() {
        return dataFimValidade;
    }

    public void setDataFimValidade(Date dataFimValidade) {
        this.dataFimValidade = dataFimValidade;
    }

    public Date getDataInicioUso() {
        return dataInicioUso;
    }

    public void setDataInicioUso(Date dataInicioUso) {
        this.dataInicioUso = dataInicioUso;
    }

    public Date getDataTerminioUso() {
        return dataTerminioUso;
    }

    public void setDataTerminioUso(Date dataTerminioUso) {
        this.dataTerminioUso = dataTerminioUso;
    }

    public int getQtdVezesAoDia() {
        return qtdVezesAoDia;
    }

    public void setQtdVezesAoDia(int qtdVezesAoDia) {
        this.qtdVezesAoDia = qtdVezesAoDia;
    }

    public String getDosagem() {
        return dosagem;
    }

    public void setDosagem(String dosagem) {
        this.dosagem = dosagem;
    }



    public void geraDose() {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(this.getDataTerminioUso());
        int a = calendar.get(Calendar.DAY_OF_YEAR);
        calendar.setTime(this.getDataInicioUso());          //Pega data de inicio e data de termino
        int b = calendar.get(Calendar.DAY_OF_YEAR);

        do {
            for(int i = 1; i <= this.getQtdVezesAoDia(); i++) {
                HorarioRemedio hora = new HorarioRemedio();
                hora.setHora(calendar.getTime());
                hora.setTomar(false);
                this.horas.add(hora);
                calendar.add(Calendar.HOUR, 24 / this.getQtdVezesAoDia());
            }
            b++;
        }while(b <= a);

    }

    public void listaHora() {    //Vai listar as horas da prescrição
        for(HorarioRemedio horaremedio : horas) {
            System.out.println(horaremedio);
        }
    }

}
