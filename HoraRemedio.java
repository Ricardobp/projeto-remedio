package Ex1;

import java.util.Date;
import java.util.LinkedList;
import java.util.Scanner;

public class HorarioRemedio {

    public Date hora;
    public boolean tomar;
    public String test;

    public Date getHora() {
        return hora;
    }

    public void setHora(Date hora) {
        this.hora = hora;
    }

    public boolean isTomar() {
        return tomar;
    }

    public void setTomar(boolean tomar) {
        this.tomar = tomar;
    }

    @Override
    public String toString() {
        return "Data e hora: \n" + getHora() + "\nTomou: " + isTomar() + "\n";
    }

}