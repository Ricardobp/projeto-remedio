package Ex1;

package excercicio1;

import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.LinkedList;
import java.util.Scanner;

public class Tela{

    private LinkedList<Prescricao> prescricao = new LinkedList<Prescricao>();   //Cria a lista de prescrições

    public void executar(int opcao) throws ParseException {
        switch(opcao){
            case 1:
                cadastrarRemedio();
                break;
            case 2:
                adicionaPrescricao();
                break;
            case 3:
                horarios();
                break;
            case 4:
                System.out.println("Tchau!!!");
                break;
            default:
                System.out.println("% Escolha uma opcao valida %");
        }
    }

    public void cadastroUsuario(){              //Menu de cadastro do usuario
        Usuario usuario = new Usuario();
        Scanner sc = new Scanner(System.in);

        System.out.println("Cadastrar Usuario");
        System.out.println("--------------------");
        System.out.print("Nome: ");
        usuario.setNome(sc.nextLine());
        System.out.print("CPF: ");
        usuario.setCpf(sc.nextLine());
        System.out.print("Email: ");
        usuario.setEmail(sc.nextLine());
    }

    public void cadastrarRemedio(){         //Menu de cadastro dos remedios
        Scanner sc = new Scanner(System.in);

        System.out.println("Cadastrar Remedio");
        System.out.println("-------------------");
        System.out.print("Nome: ");
        String name = sc.next();
        System.out.print("Laboratorio: ");
        String lab = sc.next();
        System.out.print("Principio Ativo: ");
        String pa = sc.next();

        Remedio rem = new Remedio();         //Salva os remedios na classe Remedio

        rem.setNome(name);
        rem.setLaboratorio(lab);
        rem.setPrincipioAtivo(pa);

    }

    public void mostrarTela() throws ParseException {  //Menu
        Scanner sc = new Scanner(System.in);
        int opcao = 0;
        do{
            System.out.println();
            System.out.println("Menu Remedio");
            System.out.println("------------------------");
            System.out.println("1. Cadastrar Remedio");
            System.out.println("2. Adicionar Prescricao");
            System.out.println("3. Gerar Horarios");
            System.out.println("4. Sair");
            System.out.println("------------------------");
            System.out.print("Opcao: ");
            opcao = sc.nextInt();
            executar( opcao );
        }while(opcao != 4 );
    }



    public void adicionaPrescricao() throws ParseException {  //Adiciona Prescrição
        Scanner sc = new Scanner(System.in);

        System.out.println("Prescricao");
        System.out.println("------------------");

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");    //Seta o formato para a data

        System.out.println("dd/MM/yyyy");
        System.out.print("Data de inicio: ");
        Date dinicio = df.parse(sc.next());     //Pega a data de inicio do usuario

        System.out.print("Data de termino: ");
        Date dtermino = df.parse(sc.next());   //Pega a data de termino do usuario

        System.out.print("Quantas vezes ao dia: ");
        int qtddia = sc.nextInt();              //Pega qtd de vezes ao dia

        System.out.print("Dosagem: ");
        String dosa = sc.next();            //Pega dosagem

        Prescricao pres = new Prescricao();   //Salva todos dados do usuario em prescricao
        pres.setDataInicioUso(dinicio);
        pres.setDataTerminioUso(dtermino);
        pres.setQtdVezesAoDia(qtddia);
        pres.setDosagem(dosa);
        pres.geraDose();
        prescricao.add(pres);

    }

    public void horarios() { //Lista horas
        System.out.println();
        System.out.println("Gerador de horarios");
        System.out.println("------------------------------");
        System.out.println();
        System.out.println("+-----------------------------------------+");
        System.out.println("| Voce deve tomar seu remedio nesses dias |");
        System.out.println("+-----------------------------------------+");

        for(Prescricao prescri : prescricao) {
            prescri.listaHora();
        }
        System.out.println("+-----------------------------------------+");
        System.out.println("| Voce deve tomar seu remedio nesses dias |");
        System.out.println("+-----------------------------------------+");
    }

    public static void main(String[] args) throws ParseException {  //Menu Principal
        Tela tela = new Tela();
        tela.cadastroUsuario();
        tela.mostrarTela();
    }
}
